CMC Android Tech Task
=====================

![Demo](demo.gif)

(For the demo: polling interval adjusted to 2 seconds and the app is pointed to a mock server)

Key points
----------
* View Architecture: MVVM.
* Libraries used: Dagger2, Retrofit, RxJava, Jetpack, Joda Money, Material Components, Joda Money, etc.
* App architecture designed with extensibility in mind.
* Rotation/configration changes are handled.
* Error handling is **not** afterthought.
* Use of custom views (component-based design).
* Price change animations on buy, sell, units, and amount texts.
