package com.cmcmarkets.cmcdemo

import android.app.Application
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.cmcmarkets.cmcdemo.ui.OrderActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class OrderScreenTest {

    @get:Rule
    val testRule = ActivityTestRule(OrderActivity::class.java)

    @Test
    fun appStart() {
        // TODO: Mock network/API request
        val packageName = getApplicationContext<Application>().packageName
        val uiDevice = UiDevice.getInstance(getInstrumentation())
        uiDevice.wait(Until.findObject(By.pkg(packageName).depth(0)), LAUNCH_TIMEOUT_MILLIS)
    }

    companion object {
        const val LAUNCH_TIMEOUT_MILLIS = 10_000L
    }
}
