package com.cmcmarkets.cmcdemo.repositories

import com.cmcmarkets.cmcdemo.BaseTest
import com.cmcmarkets.cmcdemo.network.Api
import com.nhaarman.mockito_kotlin.atLeastOnce
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify

class DefaultTickerRepositoryTest : BaseTest() {

    @Mock
    private lateinit var api: Api

    @InjectMocks
    private lateinit var repository: DefaultTickerRepository

    @Test
    fun `when price quotes are requested, then methods on dependencies are called`() {
        // when
        repository.getPriceQuotes()

        // then
        verify(api, atLeastOnce()).tickerPriceQuotes()
    }
}
