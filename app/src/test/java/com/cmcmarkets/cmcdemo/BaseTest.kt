package com.cmcmarkets.cmcdemo

import com.cmcmarkets.cmcdemo.util.AppRxSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.mockito.MockitoAnnotations

open class BaseTest {

    protected val trampolineSchedulers = AppRxSchedulers(
        io = Schedulers.trampoline(),
        computation = Schedulers.trampoline(),
        main = Schedulers.trampoline()
    )

    protected val testScheduler = TestScheduler()

    protected val testSchedulers = AppRxSchedulers(
        io = testScheduler,
        computation = testScheduler,
        main = testScheduler
    )

    @Before
    fun baseSetup() {
        MockitoAnnotations.initMocks(this)
    }
}