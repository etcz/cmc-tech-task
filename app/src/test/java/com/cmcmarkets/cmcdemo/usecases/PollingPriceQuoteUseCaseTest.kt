package com.cmcmarkets.cmcdemo.usecases

import com.cmcmarkets.cmcdemo.BaseTest
import com.cmcmarkets.cmcdemo.models.PriceQuote
import com.cmcmarkets.cmcdemo.models.Resource
import io.reactivex.Observable
import org.joda.money.CurrencyUnit
import org.joda.money.Money
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import java.io.IOException
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

class PollingPriceQuoteUseCaseTest : BaseTest() {

    @Mock
    private lateinit var wrappedUseCase: GetPriceQuoteUseCase

    private lateinit var pollingPriceQuoteUseCase: PollingPriceQuoteUseCase

    private val currencyCode = "AUD"

    private val currencyUnit = CurrencyUnit.of(currencyCode)

    private val priceQuote = PriceQuote(
        buyPrice = Money.of(currencyUnit, 100.0),
        sellPrice = Money.of(currencyUnit, 100.0),
        currencySymbol = "$"
    )

    @Before
    fun setup() {
        pollingPriceQuoteUseCase = PollingPriceQuoteUseCase(
            wrappedUseCase,
            POLLING_PERIOD_SECS,
            TimeUnit.SECONDS,
            testSchedulers
        )
    }

    @Test
    fun `when the use case being wrapped has an error, then upstream is re-subscribed`() {
        // given
        val requestCount = AtomicInteger()
        val networkError = IOException("Canceled")
        `when`(wrappedUseCase(anyString())).thenReturn(
            Observable.fromCallable {
                if (requestCount.incrementAndGet() < 2) {
                    Resource.Failure(networkError)
                } else {
                    Resource.Successful(priceQuote)
                }
            })

        // when
        val testObserver = pollingPriceQuoteUseCase("AUD").test()
        testScheduler.advanceTimeBy(POLLING_PERIOD_SECS + 1, TimeUnit.SECONDS)
        testScheduler.triggerActions()

        // then
        testObserver.assertValueAt(0) {
            it as Resource.Failure
            it.throwable == networkError
        }
        testObserver.assertValueAt(1) {
            it as Resource.Successful
            it.data == priceQuote
        }
    }

    companion object {
        const val POLLING_PERIOD_SECS = 5L
    }
}