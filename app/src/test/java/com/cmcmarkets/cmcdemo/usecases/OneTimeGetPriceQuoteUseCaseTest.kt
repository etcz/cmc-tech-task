package com.cmcmarkets.cmcdemo.usecases

import com.cmcmarkets.cmcdemo.BaseTest
import com.cmcmarkets.cmcdemo.models.PriceQuote
import com.cmcmarkets.cmcdemo.models.PriceQuoteDto
import com.cmcmarkets.cmcdemo.models.Resource
import com.cmcmarkets.cmcdemo.repositories.TickerRepository
import io.reactivex.Single
import org.joda.money.CurrencyUnit
import org.joda.money.Money
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import java.io.IOException

class OneTimeGetPriceQuoteUseCaseTest : BaseTest() {

    @Mock
    private lateinit var repository: TickerRepository

    private lateinit var getPriceQuoteUseCase: OneTimeGetPriceQuoteUseCase

    private val mockPriceQuotes = mapOf(
        "AUD" to PriceQuoteDto(
            fifteenMinsAgo = 8704.47,
            lastPrice = 8704.47,
            buyPrice = 8704.47,
            sellPrice = 8704.47,
            symbol = "$"
        )
    )

    @Before
    fun setup() {
        getPriceQuoteUseCase = OneTimeGetPriceQuoteUseCase(repository, testSchedulers)
    }

    @Test
    fun `when invoked and the request is successful, then proper resource is returned`() {
        // given
        val currencyUnit = CurrencyUnit.of("AUD")
        val expectedQuote = PriceQuote(
            buyPrice = Money.of(currencyUnit, 8704.47),
            sellPrice = Money.of(currencyUnit, 8704.47),
            currencySymbol = "$",
            spread = Money.zero(currencyUnit)
        )
        `when`(repository.getPriceQuotes())
            .thenReturn(Single.just(mockPriceQuotes))

        // when
        val testObserver = getPriceQuoteUseCase("AUD").test()
        testScheduler.triggerActions()

        // then
        testObserver.assertValueCount(1)
        testObserver.assertValue { resource ->
            resource as Resource.Successful
            resource.data == expectedQuote
        }
    }

    @Test
    fun `when the currency code does not have a quote, then an error resource is returned`() {
        // given
        `when`(repository.getPriceQuotes())
            .thenReturn(Single.just(mockPriceQuotes))

        // when
        val testObserver = getPriceQuoteUseCase("USD").test()
        testScheduler.triggerActions()

        // then
        testObserver.assertValueCount(1)
        testObserver.assertValue { resource ->
            resource as Resource.Failure
            resource.throwable is IllegalArgumentException
        }
    }

    @Test
    fun `when there is an upstream error, then the error is forwarded as-is`() {
        // given
        val upstreamError = IOException("Canceled")
        `when`(repository.getPriceQuotes())
            .thenReturn(Single.error(upstreamError))

        // when
        val testObserver = getPriceQuoteUseCase("AUD").test()
        testScheduler.triggerActions()

        // then
        testObserver.assertError(upstreamError)
    }
}