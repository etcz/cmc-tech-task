package com.cmcmarkets.cmcdemo.models

import org.joda.money.CurrencyUnit
import org.joda.money.Money
import org.junit.Assert.assertEquals
import org.junit.Test

class OrderTest {

    private val currencyCode = "AUD"

    private val currencyUnit = CurrencyUnit.of(currencyCode)

    private val priceQuote = PriceQuote(
        buyPrice = Money.of(currencyUnit, 100.0),
        sellPrice = Money.of(currencyUnit, 100.0),
        currencySymbol = "$"
    )

    @Test(expected = Exception::class)
    fun `when units and amount are missing, then the order is not computed`() {
        Order.Calculator()
            .quote(priceQuote)
            .action(UserAction.BUY)
            .currencyCode(currencyCode)
            .compute()
    }

    @Test(expected = Exception::class)
    fun `when currency code is missing, then the order is not computed`() {
        Order.Calculator()
            .units("100")
            .quote(priceQuote)
            .action(UserAction.BUY)
            .compute()
    }

    @Test(expected = Exception::class)
    fun `when user action is missing, then the order is not computed`() {
        Order.Calculator()
            .amount("1,000.00")
            .quote(priceQuote)
            .currencyCode(currencyCode)
            .compute()
    }

    @Test(expected = Exception::class)
    fun `when the price quote is missing, then the order is not computed`() {
        Order.Calculator()
            .units("50")
            .action(UserAction.SELL)
            .currencyCode(currencyCode)
            .compute()
    }

    @Test
    fun `when units is provided, then the correct amount is computed`() {
        val order = Order.Calculator()
            .units("5")
            .quote(priceQuote)
            .currencyCode(currencyCode)
            .action(UserAction.BUY)
            .compute()

        assertEquals(Money.of(currencyUnit, 500.0), order.amount)
        assertEquals("500.00", order.amountFormatted)
    }

    @Test
    fun `when amount is provided, then the correct number of units is computed`() {
        val order = Order.Calculator()
            .amount("1,000.00")
            .quote(priceQuote)
            .currencyCode(currencyCode)
            .action(UserAction.BUY)
            .compute()

        assertEquals(10.0.toBigDecimal().setScale(2), order.units)
        assertEquals("10", order.unitsFormatted)
    }

    @Test
    fun `when the order is computed, the it uses the correct buy or sell price quote`() {
        val spreadPositive = PriceQuote(
            buyPrice = Money.of(currencyUnit, 100.0),
            sellPrice = Money.of(currencyUnit, 200.0),
            currencySymbol = "$"
        )

        val order1 = Order.Calculator()
            .amount("1000.00")
            .quote(spreadPositive)
            .currencyCode(currencyCode)
            .action(UserAction.SELL)
            .compute()

        assertEquals(5.0.toBigDecimal().setScale(2), order1.units)
        assertEquals("5", order1.unitsFormatted)

        val spreadNegative = PriceQuote(
            buyPrice = Money.of(currencyUnit, 400.0),
            sellPrice = Money.of(currencyUnit, 200.0),
            currencySymbol = "$"
        )

        val order2 = Order.Calculator()
            .amount("1,200.00")
            .quote(spreadNegative)
            .currencyCode(currencyCode)
            .action(UserAction.BUY)
            .compute()

        assertEquals(3.0.toBigDecimal().setScale(2), order2.units)
        assertEquals("3", order2.unitsFormatted)
    }
}
