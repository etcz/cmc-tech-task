package com.cmcmarkets.cmcdemo.repositories

import com.cmcmarkets.cmcdemo.models.PriceQuoteDto
import io.reactivex.Single

interface TickerRepository {
    fun getPriceQuotes(): Single<Map<String, PriceQuoteDto>>
}