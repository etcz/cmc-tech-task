package com.cmcmarkets.cmcdemo.repositories

import com.cmcmarkets.cmcdemo.models.PriceQuoteDto
import com.cmcmarkets.cmcdemo.network.Api
import io.reactivex.Single
import javax.inject.Inject

class DefaultTickerRepository @Inject constructor(
    private val api: Api
): TickerRepository {

    override fun getPriceQuotes(): Single<Map<String, PriceQuoteDto>> {
        return api.tickerPriceQuotes()
    }
}