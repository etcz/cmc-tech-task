package com.cmcmarkets.cmcdemo.usecases

import com.cmcmarkets.cmcdemo.models.PriceQuote
import com.cmcmarkets.cmcdemo.models.Resource
import com.cmcmarkets.cmcdemo.util.AppRxSchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PollingPriceQuoteUseCase @Inject constructor(
    private val wrappedUseCase: GetPriceQuoteUseCase,
    private val period: Long,
    private val timeUnit: TimeUnit,
    private val schedulers: AppRxSchedulers
) : GetPriceQuoteUseCase {

    override fun invoke(currencyCode: String): Observable<Resource<PriceQuote>> {
        return wrappedUseCase(currencyCode)
            .onErrorReturn { Resource.Failure(it) }
            .repeatWhen { completed -> completed.delay(period, timeUnit, schedulers.computation) }
            .distinctUntilChanged()
    }
}
