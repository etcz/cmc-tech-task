package com.cmcmarkets.cmcdemo.usecases

import com.cmcmarkets.cmcdemo.models.PriceQuote
import com.cmcmarkets.cmcdemo.models.Resource
import io.reactivex.Observable

interface GetPriceQuoteUseCase {
    operator fun invoke(currencyCode: String): Observable<Resource<PriceQuote>>
}