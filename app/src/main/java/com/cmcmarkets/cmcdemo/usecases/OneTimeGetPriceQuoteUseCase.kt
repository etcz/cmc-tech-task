package com.cmcmarkets.cmcdemo.usecases

import com.cmcmarkets.cmcdemo.models.PriceQuote
import com.cmcmarkets.cmcdemo.models.PriceQuoteDto
import com.cmcmarkets.cmcdemo.models.Resource
import com.cmcmarkets.cmcdemo.repositories.TickerRepository
import com.cmcmarkets.cmcdemo.util.AppRxSchedulers
import io.reactivex.Observable
import org.joda.money.CurrencyUnit
import org.joda.money.Money
import javax.inject.Inject

class OneTimeGetPriceQuoteUseCase @Inject constructor(
    private val tickerRepository: TickerRepository,
    private val schedulers: AppRxSchedulers
) : GetPriceQuoteUseCase {

    override fun invoke(currencyCode: String): Observable<Resource<PriceQuote>> {
        return tickerRepository.getPriceQuotes()
            .toObservable()
            .subscribeOn(schedulers.io)
            .map { quotes ->
                try {
                    Resource.Successful(priceQuoteFor(quotes, currencyCode))
                } catch (e: Exception) {
                    Resource.Failure(e)
                }
            }
    }

    private fun priceQuoteFor(quotes: Map<String, PriceQuoteDto>, currencyKey: String): PriceQuote {
        val quoteDto = quotes[currencyKey]
            ?: throw IllegalArgumentException("Currency key: $currencyKey not found")
        val cu = CurrencyUnit.of(currencyKey)
        return PriceQuote(
            Money.of(cu, quoteDto.buyPrice),
            Money.of(cu, quoteDto.sellPrice),
            quoteDto.symbol
        )
    }
}