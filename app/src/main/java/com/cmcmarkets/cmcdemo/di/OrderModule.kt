package com.cmcmarkets.cmcdemo.di

import com.cmcmarkets.cmcdemo.network.Api
import com.cmcmarkets.cmcdemo.repositories.DefaultTickerRepository
import com.cmcmarkets.cmcdemo.repositories.TickerRepository
import com.cmcmarkets.cmcdemo.usecases.GetPriceQuoteUseCase
import com.cmcmarkets.cmcdemo.usecases.OneTimeGetPriceQuoteUseCase
import com.cmcmarkets.cmcdemo.usecases.PollingPriceQuoteUseCase
import com.cmcmarkets.cmcdemo.util.AppRxSchedulers
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

@Module
object OrderModule {

    @OrderScope
    @JvmStatic
    @Provides
    fun provideMainApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    @OrderScope
    @JvmStatic
    @Provides
    fun provideTickerRepository(mainApi: Api): TickerRepository {
        return DefaultTickerRepository(mainApi)
    }

    @OrderScope
    @JvmStatic
    @Provides
    fun providePriceQuoteUseCase(
        tickerRepository: TickerRepository,
        schedulers: AppRxSchedulers
    ): GetPriceQuoteUseCase {
        return PollingPriceQuoteUseCase(
            OneTimeGetPriceQuoteUseCase(tickerRepository, schedulers),
            15,
            TimeUnit.SECONDS,
            schedulers
        )
    }
}