package com.cmcmarkets.cmcdemo.di

import androidx.lifecycle.ViewModel
import com.cmcmarkets.cmcdemo.viewmodels.OrderViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class OrderViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(OrderViewModel::class)
    abstract fun bindOrderViewModel(viewModel: OrderViewModel): ViewModel
}
