package com.cmcmarkets.cmcdemo.di

import com.cmcmarkets.cmcdemo.ui.OrderActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @OrderScope
    @ContributesAndroidInjector(
        modules = [
            OrderViewModelsModule::class,
            OrderModule::class
        ]
    )
    abstract fun contributeOrderActivity(): OrderActivity
}
