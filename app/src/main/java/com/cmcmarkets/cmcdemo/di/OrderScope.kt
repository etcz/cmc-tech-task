package com.cmcmarkets.cmcdemo.di

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class OrderScope
