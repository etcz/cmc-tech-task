package com.cmcmarkets.cmcdemo.util

import com.cmcmarkets.cmcdemo.models.UserAction

const val BASE_URL = "https://blockchain.info"
const val DEFAULT_CURRENCY = "GBP"
val DEFAULT_USER_ACTION = UserAction.SELL
