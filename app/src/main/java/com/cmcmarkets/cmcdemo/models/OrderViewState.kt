package com.cmcmarkets.cmcdemo.models

import com.cmcmarkets.cmcdemo.util.DEFAULT_USER_ACTION

data class OrderViewState(
    val userAction: UserAction = DEFAULT_USER_ACTION,
    val quote: PriceQuote = PriceQuote(),
    val buyPriceFormatted: String = "",
    val sellPriceFormatted: String = "",
    val spread: String = "",
    val buyMovement: ValueMovement = ValueMovement.NONE,
    val sellMovement: ValueMovement = ValueMovement.NONE,
    val currencySymbol: String = "",
    val order: Order? = null,
    val orderInput: OrderInput? = null,
    val unitsMovement: ValueMovement = ValueMovement.NONE,
    val amountMovement: ValueMovement = ValueMovement.NONE,
    val error: Throwable? = null
)