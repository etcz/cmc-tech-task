package com.cmcmarkets.cmcdemo.models

sealed class Resource<out T> {
    data class Successful<T>(val data: T) : Resource<T>()
    data class Failure(val throwable: Throwable): Resource<Nothing>()
}