package com.cmcmarkets.cmcdemo.models

enum class InputType {
    UNITS,
    AMOUNT
}