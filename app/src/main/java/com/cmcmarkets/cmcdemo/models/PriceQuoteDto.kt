package com.cmcmarkets.cmcdemo.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PriceQuoteDto(

    @SerializedName("15m")
    @Expose
    val fifteenMinsAgo: Double,

    @SerializedName("last")
    @Expose
    val lastPrice: Double,

    @SerializedName("buy")
    @Expose
    val buyPrice: Double,

    @SerializedName("sell")
    @Expose
    val sellPrice: Double,

    @SerializedName("symbol")
    @Expose
    val symbol: String
)
