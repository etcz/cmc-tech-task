package com.cmcmarkets.cmcdemo.models

enum class ValueMovement(private val text: String, val index: Int) {
    UP("UP", -1),
    DOWN("DOWN", 1),
    NONE("NONE", 0);

    override fun toString(): String = text

    companion object {
        fun of(compareToResult: Int): ValueMovement = values().first { it.index == compareToResult }
    }
}