package com.cmcmarkets.cmcdemo.models

enum class UserAction {
    BUY,
    SELL
}