package com.cmcmarkets.cmcdemo.models

data class OrderInput(
    val type: InputType,
    val action: UserAction,
    val value: String
) {
    fun valueChanged(from: OrderInput?): Boolean {
        if (from == null) return true
        if (type != from.type) return true
        if (action != from.action) return true
        return value != from.value
    }
}
