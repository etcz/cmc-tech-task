package com.cmcmarkets.cmcdemo.models

import com.cmcmarkets.cmcdemo.extensions.printAmount
import org.joda.money.Money
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

data class Order(
    val units: BigDecimal,
    val amount: Money,
    val formatter: DecimalFormat,
    val unitsFormatted: String = formatter.format(units),
    val amountFormatted: String = amount.printAmount()
) {

    class Calculator(
        private var units: String = "",
        private var amount: String = "",
        private var quote: PriceQuote? = null,
        private var action: UserAction? = null,
        private var currencyCode: String = ""
    ) {

        fun units(units: String) = apply { this.units = units }
        fun amount(amount: String) = apply { this.amount = amount }
        fun quote(priceQuote: PriceQuote) = apply { this.quote = priceQuote }
        fun action(userAction: UserAction) = apply { this.action = userAction }
        fun currencyCode(currencyCode: String) = apply { this.currencyCode = currencyCode }

        fun compute(): Order {
            require(units.isNotBlank() || amount.isNotBlank())
            require(currencyCode.isNotBlank())
            val quote = requireNotNull(quote)
            val action = requireNotNull(action)
            val formatter = defaultFormatter(currencyCode)
            val separator = formatter.decimalFormatSymbols.groupingSeparator.toString()

            return if (units.isNotBlank()) {
                val roundedUnits = roundValue(units, separator)
                val total = unitPrice(quote, action)
                    .multipliedBy(roundedUnits, RoundingMode.HALF_EVEN)
                Order(roundedUnits, total, formatter)
            } else {
                val unitPrice = unitPrice(quote, action)
                val total = Money.of(unitPrice.currencyUnit, roundValue(amount, separator))
                val roundedUnits = total.amount.divide(unitPrice.amount, RoundingMode.FLOOR)
                Order(roundedUnits, total, formatter)
            }
        }

        private fun roundValue(input: String, separator: String): BigDecimal {
            return input
                .replace(separator, "")
                .toBigDecimal()
                .setScale(2, RoundingMode.HALF_EVEN)
        }

        private fun unitPrice(quote: PriceQuote, action: UserAction): Money {
            return when (action) {
                UserAction.BUY -> quote.buyPrice
                UserAction.SELL -> quote.sellPrice
            }
        }

        companion object {
            fun defaultFormatter(currencyCode: String): DecimalFormat {
                return NumberFormat.getInstance().apply {
                    currency = Currency.getInstance(currencyCode)
                } as DecimalFormat
            }
        }
    }
}
