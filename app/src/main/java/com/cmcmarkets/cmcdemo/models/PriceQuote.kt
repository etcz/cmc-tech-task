package com.cmcmarkets.cmcdemo.models

import com.cmcmarkets.cmcdemo.util.DEFAULT_CURRENCY
import org.joda.money.CurrencyUnit
import org.joda.money.Money

data class PriceQuote(
    val buyPrice: Money = Money.zero(CurrencyUnit.of(DEFAULT_CURRENCY)),
    val sellPrice: Money = Money.zero(CurrencyUnit.of(DEFAULT_CURRENCY)),
    val currencySymbol: String = "",
    val spread: Money = sellPrice - buyPrice,
    val buyMovement: ValueMovement = ValueMovement.NONE,
    val sellMovement: ValueMovement = ValueMovement.NONE
)
