package com.cmcmarkets.cmcdemo.viewmodels

import androidx.lifecycle.MutableLiveData
import com.cmcmarkets.cmcdemo.extensions.printAmount
import com.cmcmarkets.cmcdemo.models.*
import com.cmcmarkets.cmcdemo.usecases.GetPriceQuoteUseCase
import com.cmcmarkets.cmcdemo.util.DEFAULT_CURRENCY
import com.cmcmarkets.cmcdemo.util.DEFAULT_USER_ACTION
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class OrderViewModel @Inject constructor(
    getPriceQuoteUseCase: GetPriceQuoteUseCase
) : BaseViewModel() {

    private val orderInputSubject: BehaviorSubject<OrderInput> =
        BehaviorSubject.createDefault(OrderInput(InputType.UNITS, DEFAULT_USER_ACTION, ""))

    val viewStateLiveData: MutableLiveData<OrderViewState> = MutableLiveData(OrderViewState())

    init {
        val disposable = Observable.combineLatest(
            orderInputSubject,
            getPriceQuoteUseCase(DEFAULT_CURRENCY),
            BiFunction<OrderInput, Resource<PriceQuote>, Pair<OrderInput, Resource<PriceQuote>>> { t1, t2 ->
                Pair(t1, t2)
            }
        ).map { (orderInput, resource) ->
            if (resource is Resource.Failure) {
                currentViewState().copy(error = resource.throwable, orderInput = orderInput)
            } else {
                nextViewState(orderInput, resource)
            }
        }.scan { prev, next ->
            computePriceMovements(prev, next)
        }.subscribe({
            viewStateLiveData.postValue(it)
        }) {
            val errorState = currentViewState().copy(error = it)
            viewStateLiveData.postValue(errorState)
        }
        disposables.add(disposable)
    }

    fun setBuyOption() {
        orderInputSubject.onNext(currentOrderInput().copy(action = UserAction.BUY))
    }

    fun setSellOption() {
        orderInputSubject.onNext(currentOrderInput().copy(action = UserAction.SELL))
    }

    fun updateAmount(numUnits: String) {
        val orderInput = if (numUnits.isBlank()) {
            currentOrderInput().copy(type = InputType.UNITS, value = "")
        } else {
            currentOrderInput().copy(type = InputType.UNITS, value = numUnits)
        }
        orderInputSubject.onNext(orderInput)
    }

    fun updateUnits(totalAmount: String) {
        val orderInput = if (totalAmount.isBlank()) {
            currentOrderInput().copy(type = InputType.AMOUNT, value = "")
        } else {
            currentOrderInput().copy(type = InputType.AMOUNT, value = totalAmount)
        }
        orderInputSubject.onNext(orderInput)
    }

    private fun currentOrderInput() = orderInputSubject.value!!

    private fun currentViewState() = viewStateLiveData.value!!

    private fun nextViewState(
        orderInput: OrderInput,
        resource: Resource<PriceQuote>
    ): OrderViewState {
        resource as Resource.Successful

        val quote = resource.data
        val viewState = OrderViewState(
            orderInput.action,
            quote,
            quote.buyPrice.printAmount(),
            quote.sellPrice.printAmount(),
            quote.spread.printAmount(),
            quote.buyMovement,
            quote.sellMovement,
            quote.currencySymbol
        )

        if (orderInput.value.isBlank()) return viewState

        val calculator = Order.Calculator()
            .quote(quote)
            .action(orderInput.action)
            .currencyCode(DEFAULT_CURRENCY)

        val order = if (orderInput.type == InputType.UNITS) {
            calculator.units(orderInput.value)
        } else {
            calculator.amount(orderInput.value)
        }.compute()

        return viewState.copy(error = null, order = order, orderInput = orderInput)
    }

    private fun computePriceMovements(prev: OrderViewState, next: OrderViewState): OrderViewState {
        val buyComparison = prev.quote.buyPrice.compareTo(next.quote.buyPrice)
        val sellComparison = prev.quote.sellPrice.compareTo(next.quote.sellPrice)

        if (prev.order == null || next.order == null) {
            return next.copy(
                buyMovement = ValueMovement.of(buyComparison),
                sellMovement = ValueMovement.of(sellComparison)
            )
        }

        prev.orderInput?.let { prevOrderInput ->
            if (prevOrderInput.valueChanged(next.orderInput)) {
                return next.copy(
                    buyMovement = ValueMovement.of(buyComparison),
                    sellMovement = ValueMovement.of(sellComparison)
                )
            }
        }

        val unitsComparison = prev.order.units.compareTo(next.order.units)
        val amountComparison = prev.order.amount.compareTo(next.order.amount)

        return next.copy(
            buyMovement = ValueMovement.of(buyComparison),
            sellMovement = ValueMovement.of(sellComparison),
            unitsMovement = ValueMovement.of(unitsComparison),
            amountMovement = ValueMovement.of(amountComparison)
        )
    }
}
