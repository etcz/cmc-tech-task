package com.cmcmarkets.cmcdemo.network

import com.cmcmarkets.cmcdemo.models.PriceQuoteDto
import io.reactivex.Single
import retrofit2.http.GET

interface Api {

    @GET("/ticker")
    fun tickerPriceQuotes(): Single<Map<String, PriceQuoteDto>>
}