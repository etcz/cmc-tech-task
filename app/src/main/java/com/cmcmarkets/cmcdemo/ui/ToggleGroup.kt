package com.cmcmarkets.cmcdemo.ui

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.children
import com.cmcmarkets.cmcdemo.extensions.find

class ToggleGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    var listener: Listener? = null

    private val boundsMap: HashMap<Rect, View> = hashMapOf()

    override fun onFinishInflate() {
        super.onFinishInflate()
        boundsMap.clear()
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        val numSelected = children
            .filterIsInstance<SelectableView>()
            .count { it.isSelected() }
        if (numSelected > 1) throw IllegalStateException(ILLEGAL_SELECTION_STATE)
        computeBounds()
    }

    override fun addView(child: View?) {
        if (child !is SelectableView) throw IllegalArgumentException(ILLEGAL_ARGUMENT_MESSAGE)
        super.addView(child)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (ev == null) return true
        if (ev.actionMasked == MotionEvent.ACTION_DOWN) {
            val x = ev.x.toInt()
            val y = ev.y.toInt()
            findDescendantAt(x, y)?.let { target ->
                toggleSelection(target as SelectableView)
            }
        }
        return false
    }

    fun select(view: SelectableView) {
        children
            .filterIsInstance<SelectableView>()
            .forEach { it.select(false) }
        view.select(true)
    }

    private fun computeBounds() {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val bounds = Rect()
            child.getHitRect(bounds)
            bounds.offset(left, top)
            boundsMap[bounds] = child
        }
    }

    private fun toggleSelection(newSelection: SelectableView) {
        select(newSelection)
        listener?.onToggle(newSelection)
    }

    private fun findDescendantAt(x: Int, y: Int): View? =
        boundsMap.find { rect, _ -> rect.contains(x, y) }

    interface SelectableView {
        fun isSelected(): Boolean
        fun select(selected: Boolean)
        fun selectedColor(): Int
    }

    interface Listener {
        fun onToggle(view: SelectableView)
    }

    companion object {
        private const val ILLEGAL_ARGUMENT_MESSAGE = "The child view must be a ToggleableView."
        private const val ILLEGAL_SELECTION_STATE = "Only a single child can be selected."
    }
}