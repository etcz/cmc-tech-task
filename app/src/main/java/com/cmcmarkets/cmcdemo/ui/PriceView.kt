package com.cmcmarkets.cmcdemo.ui

import android.content.Context
import android.text.Spannable
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.withStyledAttributes
import androidx.core.text.toSpannable
import com.cmcmarkets.cmcdemo.R
import com.cmcmarkets.cmcdemo.extensions.animateTextColor
import com.cmcmarkets.cmcdemo.extensions.getColorCompat
import com.cmcmarkets.cmcdemo.models.ValueMovement
import kotlinx.android.synthetic.main.layout_price_view.view.*
import java.util.*

class PriceView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs), ToggleGroup.SelectableView {

    private val relativeSizeSpan = RelativeSizeSpan(DECIMAL_RELATIVE_SIZE)

    private val colorOnPrimary by lazy { context.getColorCompat(R.color.colorOnPrimary) }
    private val greenColor by lazy { context.getColorCompat(R.color.light_green) }
    private val redColor by lazy { context.getColorCompat(R.color.bright_red) }

    private var selectedBgColor = -1
    private var normalBgColor = -1
    private var headerTextColor = -1
    private var isViewSelected = false

    private var price: String = ""
        set(value) {
            field = value
            priceValue.text = formatPrice(value)
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_price_view, this)
        context.withStyledAttributes(attrs, R.styleable.PriceView) {
            val locale = Locale.getDefault()
            headerTextColor = getColor(R.styleable.PriceView_headerTextColor, -1)
            selectedBgColor = getColor(R.styleable.PriceView_selectedBackgroundColor, -1)
            normalBgColor = getColor(R.styleable.PriceView_normalBackgroundColor, -1)
            isViewSelected = getBoolean(R.styleable.PriceView_selected, false)
            header.text = getString(R.styleable.PriceView_headerText)?.toUpperCase(locale)
            header.setTextColor(headerTextColor)
            priceValue.text = getString(R.styleable.PriceView_priceText)?.toUpperCase(locale)
            val alignment = getInt(R.styleable.PriceView_headerTextAlignment, TextAlignment.START.ordinal)
            alignHeaderText(alignment)
            select(isViewSelected)
        }
    }

    override fun isSelected(): Boolean = isViewSelected

    override fun select(selected: Boolean) {
        isViewSelected = selected
        setBackgroundColor(if (isViewSelected) selectedBgColor else normalBgColor)
    }

    override fun selectedColor(): Int = headerTextColor

    fun bind(text: String, movement: ValueMovement) {
        price = text
        when (movement) {
            ValueMovement.NONE -> priceValue.setTextColor(colorOnPrimary)
            ValueMovement.DOWN -> priceValue.animateTextColor(ANIM_DURATION_MILLIS, colorOnPrimary, redColor)
            ValueMovement.UP -> priceValue.animateTextColor(ANIM_DURATION_MILLIS, colorOnPrimary, greenColor)
        }
    }

    private fun formatPrice(text: String): Spannable {
        val decimalPointIdx = text.indexOf(".")
        if (decimalPointIdx == -1) return text.toSpannable()
        return text.toSpannable().apply {
            setSpan(relativeSizeSpan, decimalPointIdx, text.length, SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }

    private fun alignHeaderText(alignment: Int) {
        val gravity = when (alignment) {
            TextAlignment.START.ordinal -> Gravity.START
            TextAlignment.CENTER.ordinal -> Gravity.CENTER
            TextAlignment.END.ordinal -> Gravity.END
            else -> Gravity.START
        }
        val lp = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        lp.gravity = gravity
        header.layoutParams = lp
    }

    enum class TextAlignment {
        START,
        CENTER,
        END;
    }

    companion object {
        private const val DECIMAL_RELATIVE_SIZE = 0.8f
        private const val ANIM_DURATION_MILLIS = 400L
    }
}