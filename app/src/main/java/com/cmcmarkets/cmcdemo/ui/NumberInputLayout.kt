package com.cmcmarkets.cmcdemo.ui

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.withStyledAttributes
import com.cmcmarkets.cmcdemo.R
import com.cmcmarkets.cmcdemo.extensions.animateTextColor
import com.cmcmarkets.cmcdemo.extensions.getColorCompat
import com.cmcmarkets.cmcdemo.models.ValueMovement
import kotlinx.android.synthetic.main.layout_number_input.view.*

class NumberInputLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    var descriptionText: String = ""
        set(value) {
            field = value
            description.text = value
        }

    var descriptionTextColor: Int = -1
        set(value) {
            field = value
            description.setTextColor(value)
        }

    private val colorOnPrimary by lazy { context.getColorCompat(R.color.colorOnPrimary) }
    private val greenColor by lazy { context.getColorCompat(R.color.light_green) }
    private val redColor by lazy { context.getColorCompat(R.color.bright_red) }

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_number_input, this)
        context.withStyledAttributes(attrs, R.styleable.NumberInputLayout) {
            descriptionText = getString(R.styleable.NumberInputLayout_descriptionText) ?: ""
            descriptionTextColor = getColor(R.styleable.NumberInputLayout_descriptionTextColor, -1)
        }
    }


    override fun onSaveInstanceState(): Parcelable? {
        return Bundle().apply {
            putParcelable(BUNDLE_SUPER_STATE, super.onSaveInstanceState())
            putBoolean(BUNDLE_IS_FOCUSED, numberInput.isFocused)
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is Bundle) {
            val wasFocused = state.getBoolean(BUNDLE_IS_FOCUSED, false)
            if (wasFocused) post { numberInput.requestFocus() }
            super.onRestoreInstanceState(state.getParcelable(BUNDLE_SUPER_STATE))
        }
    }

    fun bind(description: String, value: String?, movement: ValueMovement) {
        if (description.isNotBlank()) descriptionText = description
        numberInput.setText(value)
        val endColor = when (movement) {
            ValueMovement.NONE-> colorOnPrimary
            ValueMovement.DOWN -> redColor
            ValueMovement.UP -> greenColor
        }
        numberInput.animateTextColor(ANIM_DURATION_MILLIS, colorOnPrimary, endColor)
    }

    companion object {
        private const val BUNDLE_SUPER_STATE = "BUNDLE_SUPER_STATE"
        private const val BUNDLE_IS_FOCUSED = "BUNDLE_IS_FOCUSED"
        private const val ANIM_DURATION_MILLIS = 200L
    }
}