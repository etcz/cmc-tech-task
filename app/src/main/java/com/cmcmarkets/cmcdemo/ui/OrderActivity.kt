package com.cmcmarkets.cmcdemo.ui

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cmcmarkets.cmcdemo.R
import com.cmcmarkets.cmcdemo.extensions.hideSoftKeyboard
import com.cmcmarkets.cmcdemo.extensions.shortToast
import com.cmcmarkets.cmcdemo.extensions.viewModelProvider
import com.cmcmarkets.cmcdemo.models.OrderViewState
import com.cmcmarkets.cmcdemo.models.UserAction
import com.cmcmarkets.cmcdemo.viewmodels.OrderViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_number_input.view.*
import javax.inject.Inject

class OrderActivity : DaggerAppCompatActivity(), View.OnFocusChangeListener, View.OnKeyListener,
    ToggleGroup.Listener {

    @Inject
    lateinit var providerFactory: ViewModelProvider.Factory

    private lateinit var viewModel: OrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        viewModel = viewModelProvider(providerFactory)
        viewModel.viewStateLiveData.observe(this, Observer(::bind))
    }

    override fun onToggle(view: ToggleGroup.SelectableView) {
        setColorOfOtherComponents(view)
        when (view) {
            sellPriceView -> viewModel.setSellOption()
            buyPriceView -> viewModel.setBuyOption()
        }
    }

    override fun onFocusChange(view: View, hasFocus: Boolean) {
        if (!hasFocus) {
            view as EditText
            handleInputCompletion(view)
        }
    }

    override fun onKey(view: View, keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
            hideSoftKeyboard()
            view as EditText
            handleInputCompletion(view)
            return true
        }
        return false
    }

    private fun initViews() {
        toggleGroupLayout.listener = this
        unitsInput.numberInput.setOnFocusChangeListener(this)
        unitsInput.numberInput.setOnKeyListener(this)
        amountInput.numberInput.setOnFocusChangeListener(this)
        amountInput.numberInput.setOnKeyListener(this)
        confirmButton.setOnClickListener { shortToast(getString(R.string.order_confirmed)) }
        cancelButton.setOnClickListener { finish() }
    }

    private fun setColorOfOtherComponents(sourceView: ToggleGroup.SelectableView) {
        val color = sourceView.selectedColor()
        unitsInput.descriptionTextColor = color
        amountInput.descriptionTextColor = color
        confirmButton.setBackgroundColor(color)
    }

    private fun bind(viewState: OrderViewState) {
        if (viewState.error != null) {
            println("viewState.error: ${viewState.error}")
            shortToast(getString(R.string.price_update_error_message))
            return
        }

        val priceView = when (viewState.userAction) {
            UserAction.SELL -> sellPriceView
            UserAction.BUY -> buyPriceView
        }
        toggleGroupLayout.select(priceView)
        setColorOfOtherComponents(priceView)

        with(viewState) {
            sellPriceView.bind(sellPriceFormatted, sellMovement)
            buyPriceView.bind(buyPriceFormatted, buyMovement)

            spreadValue.text = spread

            val description = getString(R.string.amount_with_currency, currencySymbol)
            val amountText = order?.amountFormatted
            amountInput.bind(description, amountText, amountMovement)
            val unitsText = order?.unitsFormatted
            unitsInput.bind("", unitsText, unitsMovement)

            confirmButton.isEnabled = !(amountText.isNullOrBlank() || unitsText.isNullOrBlank())
        }
    }

    private fun handleInputCompletion(editText: EditText) {
        val inputText = editText.text.toString()
        when (editText) {
            unitsInput.numberInput -> viewModel.updateAmount(inputText)
            amountInput.numberInput -> viewModel.updateUnits(inputText)
        }
    }
}
