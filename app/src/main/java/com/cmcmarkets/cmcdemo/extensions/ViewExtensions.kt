package com.cmcmarkets.cmcdemo.extensions

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.widget.TextView

fun TextView.animateTextColor(millis: Long, startColor: Int, endColor: Int) {
    val valueAnimator = ObjectAnimator.ofInt(
        this,
        "textColor",
        startColor,
        endColor
    )

    valueAnimator.run {
        setEvaluator(ArgbEvaluator())
        duration = millis
        repeatCount = 1
        repeatMode = ValueAnimator.REVERSE
        start()
    }
}
