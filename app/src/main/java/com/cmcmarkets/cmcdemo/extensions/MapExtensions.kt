package com.cmcmarkets.cmcdemo.extensions

fun <K, V> Map<K, V>.find(predicate: (K, V) -> Boolean): V? {
    for ((key, value) in this) {
        if (predicate(key, value)) {
            return value
        }
    }
    return null
}
