package com.cmcmarkets.cmcdemo.extensions

import android.animation.Animator
import android.view.ViewPropertyAnimator

fun ViewPropertyAnimator.setEndListener(listener: () -> Unit) {
    setListener(object : android.animation.AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator?) {
            super.onAnimationEnd(animation)
            listener.invoke()
        }
    })
}