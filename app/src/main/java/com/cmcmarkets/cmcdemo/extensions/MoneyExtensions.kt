package com.cmcmarkets.cmcdemo.extensions

import org.joda.money.Money
import org.joda.money.format.MoneyFormatterBuilder

fun Money.printAmount(): String {
    return MoneyFormatterBuilder()
        .appendAmount()
        .toFormatter()
        .print(this)
}

